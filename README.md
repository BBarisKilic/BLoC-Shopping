# BLoC Shopping

An app that uses 'Business Logic Component' design pattern and bloc state management.

## Through this project, I've learned:

-   bloc design pattern and state management.
-   singleton and factory design pattern.